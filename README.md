# README #

Describing usage of this app.

### How do I get set up? ###

## Environment Prerequisites ##
jdk 8, wildfly 10, git, maven, mysql

## Mysql prerequisites ##
When installing, remember the root password. Let's say that it is "MyPassword"
Create Database called 'agriculture'. Ex: mysql -uroot -pMyPassword; create database agriculture;

## Wildfly prerequisites ##
add JBOSS_HOME and JAVA_HOME to /etc/environment
Ex: JBOSS_HOME="/home/pi/wildfly/"
	JAVA_HOME="/opt/jdk8/"
TODO..
## Deployment instructions ##
Edit crontab to start jboss at startup:
sudo crontab -e
@reboot /home/pi/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 &

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact