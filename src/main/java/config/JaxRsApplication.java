package config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@ApplicationPath("/rest/")
public class JaxRsApplication extends Application{
    //map all classes by default annotated with @Path
	//otherwise implement the Application getSingletons and getClasses methods
}
