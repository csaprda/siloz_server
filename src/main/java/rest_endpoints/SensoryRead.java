package rest_endpoints;

import api.DAOWrapperLocal;
import api.Sort;
import model.SensorReading;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Path("/reading")
public class SensoryRead {

    @EJB
    private DAOWrapperLocal dao;

    @Inject
    private Logger log;

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response persistSensorReadings(
            Collection<SensorReading> sensorReadings) {
        if (sensorReadings == null || sensorReadings.isEmpty()) {
            log.error("Tried to persist null or empty Collection<SensorReading>");
            return Response.ok().status(Status.NO_CONTENT).build();
        }
        boolean successfulPersist = dao.persist(sensorReadings);
        Status status;
        if (successfulPersist) {
            status = Status.CREATED;
        } else {
            status = Status.NOT_MODIFIED;
        }
        return Response.ok().status(status).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response persistSensorReading(@PathParam("id") String id,
                                         SensorReading sensorReading) {
        if (sensorReading == null) {
            log.error("Tried to persist null SensorReading with id {}", id);
            return Response.ok().status(Status.NO_CONTENT).build();
        }

        boolean successfulPersist = dao.persist(sensorReading);
        Status status;
        if (successfulPersist) {
            status = Status.CREATED;
        } else {
            status = Status.NOT_MODIFIED;
        }
        return Response.ok().status(status).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        try {
            Status status;
            @SuppressWarnings("unchecked")
            Collection<SensorReading> list = (Collection<SensorReading>) dao.getAllSorted(SensorReading.class, "date", Sort.DESC);
            if (list.size() == 0) {
                status = Status.NO_CONTENT;
            } else {
                status = Status.OK;
            }
            return Response.ok().entity(list).status(status).build();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return Response.ok().status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/between")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBetweenDates(@QueryParam("from") Date fromDate, @QueryParam("to") Date toDate) {
        Status status;
        @SuppressWarnings("unchecked") List<SensorReading> result =
                (List<SensorReading>) dao.getReadingsBetweenDatesSorted(SensorReading.class, fromDate, toDate, "date", Sort.DESC);
        if (result.size() == 0) {
            status = Status.NO_CONTENT;
        } else {
            status = Status.OK;
        }
        return Response.ok().entity(result).status(status).build();
    }
}
