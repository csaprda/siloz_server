package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-08-07T21:08:41.240+0100")
@StaticMetamodel(SensorReading.class)
public class SensorReading_ {
	public static volatile SingularAttribute<SensorReading, String> id;
	public static volatile SingularAttribute<SensorReading, Short> sensorId;
	public static volatile SingularAttribute<SensorReading, Long> dateEpoch;
	public static volatile SingularAttribute<SensorReading, Date> readDate;
	public static volatile SingularAttribute<SensorReading, Double> sensorHeight;
	public static volatile SingularAttribute<SensorReading, Boolean> sensorActive;
	public static volatile SingularAttribute<SensorReading, Double> readTemperature;
	public static volatile SingularAttribute<SensorReading, Double> readHumidity;
}
