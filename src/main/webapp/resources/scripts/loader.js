define("loader", ['jquery'], function ($) {

    var me = {
        show: function () {
            if (document.getElementById("spinner") === null) {
                $("body").append('<div id="spinner"></div>');
            }
            document.getElementById("spinner").style.display = 'block';
        },

        hide: function () {
            if (document.getElementById("spinner") !== null) {
                document.getElementById("spinner").style.display = 'none';
            }
        }
    }

    return me;
});