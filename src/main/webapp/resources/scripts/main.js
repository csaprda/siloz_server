requirejs.config({
    baseUrl: 'resources/scripts/lib',
    paths: {
        jquery: 'jquery',
        util: '../util',
        loader: '../loader',
        ReadingCollection: '../ReadingCollection',
        modernizr: 'modernizr',
        datatables: 'jquery.dataTables.min',
        chart: 'Chart',
        slick: 'slick',
        "jquery-ui": 'jquery-ui'
    }
});

require(['jquery', 'util', 'tableUtil', 'loader'], function ($, util, tableUtil, loader) {
    util.loadCSS(window.location.href);
    util.addDetailsFunctionality();
    loader.show();
    tableUtil.prepare();
    tableUtil.loadTable();
    loader.hide();
});

define("tableUtil", ['jquery', 'jquery-ui', 'datatables', 'util', 'chartjs'], function ($, ui, datatables, util, chartjs) {

    var me = {
        prepare: function () {
            $('#dataTable').html('<div style="display:inline-flex">De la: <input type="text" id="datepicker"> Pana la: <input type="text"' +
                ' id="datepicker1"><input type="button" id="redrawTable" value="Reîmprospătează"></div>' +
                '<table cellspacing="0" id="queryResults"></table>');
            $('#redrawTable').click(function () {
                me.loadTable();
            });
            from = $("#datepicker");
            from.datepicker();
            from.datepicker('setDate', new Date(new Date().setDate(new Date().getDate() - 30)));
            to = $("#datepicker1");
            to.datepicker();
            to.datepicker('setDate', new Date());
        },

        loadTable: function () {
            var fromDate = from.datepicker('getDate');
            var toDate = to.datepicker('getDate');
            if (fromDate < toDate) {
                toDate.setHours(23, 59, 59);
            } else {
                fromDate.setHours(23, 59, 59);
            }
            $.ajax({
                url: "rest/reading/between",
                type: "GET",
                data: {
                    "from": fromDate,
                    "to": toDate
                },
                dataType: "json",
                success: function (response) {
                    if (response === undefined) {
                        alert("Baza de date este goala!");
                        return;
                    }
                    // calls createChart after 0ms, but lets the flow continue.
                    // ASYNC functionality
                    charts = $("#charts");
                    charts.replaceWith('<div id="charts"></div>');
                    $("#chartsMonthly").replaceWith('<div id="chartsMonthly"></div>');
                    setTimeout(chartjs.createChart(response), 0);
                    queryResults = $("#queryResults");
                    if (queryResults.find("tr").length === 0) {
                        setTimeout(me.createTable(response), 0);
                    } else {
                        queryResults.replaceWith('<div id="dataTable"></div>');
                        myTable.fnDestroy();
                        setTimeout(me.createTable(response), 0);
                    }
                },
                error: function (message) {
                    document.write(message.responseText);
                }
            });
        },

        createTable: function (response) {
            console.log("Building table with %s elements", response.length);
            myTable = queryResults.dataTable({
                language: util.tableLanguage,
                data: response,
                bAutoWidth: false,
                bPaginate: true,
                deferRender: true,
                search: true,
                sort: true,
                order: [[2, "desc"]],
                columns: util.tableColumns,
                aoColumnDefs: [{
                    "sDefaultContent": '-',
                    "aTargets": ['_all']
                }]
            });
            console.log("Table created successfully");
        }
    }

    return me;
});

define("chartjs", ['jquery', 'chart', 'carousel', 'ReadingCollection'], function ($, Chart, carousel,
                                                                                  ReadingCollection) {
    var me = {
        createChart: function (responseData) {
            var chartData = responseData;
            chartData.sort(function(a, b){return a.date-b.date});
            console.log("Creating charts...");
            var lastMonthData = me.extractLastMonthData(chartData);
            var last24hData = me.extract24hData(lastMonthData);
            if (lastMonthData.length == 0) {
                console.log("No data for charts");
                return;
            }
            me.extractSensors(lastMonthData);
            me.extractDayLabels(last24hData);
            me.extractMonthLabels(lastMonthData);

            if (last24hData.length != 0) {
                //create chart for day view
                var dayDatasets = me.makeChartDatasetForDailyData(last24hData);
                for (var i = 0; i < me.sensors.length; i++) {
                    var data = {
                        labels: me.dayLabels,
                        datasets: dayDatasets[i]
                    };
                    $('<div><p class="chartTitle">' + me.sensors[i] + '</p><canvas id="chart' + i + '" width="1000" height="500"></canvas></div>')
                        .appendTo('#charts');
                    var ctx = $("#chart" + i).get(0).getContext("2d");
                    var myNewChart = new Chart(ctx).Line(data, {
                        multiTooltipTemplate: "<%= value %> <%= datasetLabel %>",
                        scaleFontSize: 12,
                        scaleFontStyle: "normal",
                        scaleFontColor: "#fff",
                        animation: false
                    });
                }
            }

            //create chart for month view
            var monthDatasets = me.makeChartDatasetForMonthyData(lastMonthData);
            for (var i = 0; i < me.sensors.length; i++) {
                var data = {
                    labels: me.monthLabels,
                    datasets: monthDatasets[i]
                };
                $('<div><p class="chartTitle">' + me.sensors[i] + '</p><canvas id="monthlyChart' + i + '" width="1000" height="500"></canvas></div>')
                    .appendTo('#chartsMonthly');
                var ctx = $("#monthlyChart" + i).get(0).getContext("2d");
                var myNewChart = new Chart(ctx).Line(data, {
                    multiTooltipTemplate: "<%= value %> <%= datasetLabel %>",
                    scaleFontSize: 12,
                    scaleFontStyle: "normal",
                    scaleFontColor: "#fff",
                    animation: false
                });
            }

            console.log("Charts created successfully");
            carousel.load();
        },

        extractLastMonthData: function (responseData) {
            var biggestDate = new Date(Math.max.apply(Math, responseData.map(function (o) {
                return o.date;
            })));
            var pastThirtyDays = biggestDate - 30 * 24 * 60 * 60 * 1000;
            return responseData.filter(function (value) {
                if (value.date >= pastThirtyDays) {
                    return value;
                }
            });
        },

        extract24hData: function (responseData) {
            var biggestDate = new Date(Math.max.apply(Math, responseData.map(function (o) {
                return o.date;
            })));
            var yesterday = biggestDate - 24 * 60 * 60 * 1000;
            return responseData.filter(function (value) {
                if (value.date >= yesterday) {
                    return value;
                }
            });
        },

        extractDayLabels: function (data) {
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "Mai", "Iun", "Iul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var set = new Set();
            for (var i = 0; i < data.length; i++) {
                var mdate = new Date(data[i].date);
                var hour = mdate.getHours();
                var hoursString = hour < 10 ? "0" + hour : hour;
                set.add(mdate.getDate() + " " + monthNames[mdate.getMonth()] + " " + hoursString + ":00");
            }
            me.dayLabels = [];
            set.forEach(function (value) {
                me.dayLabels.push(value);
            });
        },

        extractMonthLabels: function (data) {
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "Mai", "Iun", "Iul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var set = new Set();
            for (var i = 0; i < data.length; i++) {
                var mdate = new Date(data[i].date);
                set.add(mdate.getDate() + " " + monthNames[mdate.getMonth()]);
            }
            me.monthLabels = [];
            set.forEach(function (value) {
                me.monthLabels.push(value);
            });
        },

        extractSensors: function (data) {
            var set = new Set();
            for (var i = 0; i < data.length; i++) {
                set.add(data[i].sensorId);
            }
            me.sensors = [];
            set.forEach(function (value) {
                me.sensors.push(value);
            });
        },

        makeChartDatasetForMonthyData: function (mdata) {
            var dataset = [];
            for (var i = 0; i < me.sensors.length; i++) {
                var readingCollection = new ReadingCollection();
                mdata.filter(function (object) {
                    if (object.sensorId == me.sensors[i])
                        readingCollection.add(object);
                });

                var temperatureDataForThisSensor = [];
                var maxTemperatureDataForThisSensor = [];
                var minTemperatureDataForThisSensor = [];
                var humidityDataForThisSensor = [];
                temperatureDataForThisSensor = readingCollection.getAveragedReadingsCollection().map(function (object) {
                    return object.temperature;
                });
                maxTemperatureDataForThisSensor = readingCollection.getMaxTemperatureData();
                minTemperatureDataForThisSensor = readingCollection.getMinTemperatureData();
                humidityDataForThisSensor = readingCollection.getAveragedReadingsCollection().map(function (object) {
                    return object.humidity;
                });
                dataset.push(
                    [{
                        label: "Max ℃",
                        fillColor: "rgba(220,220,220,0.0)",
                        strokeColor: "red",
                        pointColor: "red",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: maxTemperatureDataForThisSensor
                    }, {
                        label: "Med ℃",
                        fillColor: "rgba(220,220,220,0.0)",
                        strokeColor: "orange",
                        pointColor: "orange",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: temperatureDataForThisSensor
                    }, {
                        label: "Min ℃",
                        fillColor: "rgba(220,220,220,0.0)",
                        strokeColor: "blue",
                        pointColor: "blue",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: minTemperatureDataForThisSensor
                    }, {
                        label: "%",
                        fillColor: "rgba(220,220,220,0.0)",
                        strokeColor: "yellow",
                        pointColor: "yellow",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: humidityDataForThisSensor
                    }]);
            }
            return dataset;
        },

        makeChartDatasetForDailyData: function (mdata) {
            var dataset = [];
            for (var i = 0; i < me.sensors.length; i++) {
                var readingCollection = new ReadingCollection();
                mdata.filter(function (object) {
                    if (object.sensorId == me.sensors[i])
                        readingCollection.add(object);
                })

                var temperatureDataForThisSensor = [];
                var humidityDataForThisSensor = [];
                temperatureDataForThisSensor = readingCollection.getLast24hData().map(function (object) {
                    return object.temperature;
                });
                humidityDataForThisSensor = readingCollection.getLast24hData().map(function (object) {
                    return object.humidity;
                });
                dataset.push(
                    [{
                        label: "℃",
                        fillColor: "rgba(220,220,220,0.0)",
                        strokeColor: "red",
                        pointColor: "red",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: temperatureDataForThisSensor
                    }, {
                        label: "%",
                        fillColor: "rgba(220,220,220,0.0)",
                        strokeColor: "yellow",
                        pointColor: "yellow",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: humidityDataForThisSensor
                    }]);
            }
            return dataset;
        },

        sensors: [],
        monthLabels: [],
        dayLabels: []
    }
    return me;
});

define("carousel", ['slick', 'jquery'], function (Slick, $) {
    var me = {
        load: function () {
            $('#charts').slick({
                dots: true,
                infinite: true,
                speed: 400,
                fade: true,
                cssEase: 'linear'
            });

            $('#chartsMonthly').slick({
                dots: true,
                infinite: true,
                speed: 400,
                fade: true,
                cssEase: 'linear'
            });
        }
    }
    return me;
});
